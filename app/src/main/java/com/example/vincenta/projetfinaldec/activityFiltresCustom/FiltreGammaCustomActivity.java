package com.example.vincenta.projetfinaldec.activityFiltresCustom;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.vincenta.projetfinaldec.R;

public class FiltreGammaCustomActivity extends AppCompatActivity {

    EditText parametre1;
    EditText parametre2;
    EditText parametre3;
    Ecouteur ec;

    ImageButton btnOkGammaCustom;
    ImageButton btnCancelGammaCustom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtre_gamma_custom);
        setupVariables();
    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {

            if(v==btnOkGammaCustom){
                if(champsVide()){
                    Toast.makeText(getApplicationContext(),"Remplissez tous les champs",Toast.LENGTH_LONG).show();
                }
                else{
                    double[] lesParams=creationParams();
                    Intent i=new Intent();
                    i.putExtra("lesParams",lesParams);
                    i.putExtra("choix","ok");
                    setResult(RESULT_OK, i);
                    finish();
                }
            }
            if(v==btnCancelGammaCustom){
                Intent i=new Intent();
                i.putExtra("choix","cancel");
                setResult(RESULT_OK,i);
                finish();
            }
        }
    }

    private boolean champsVide(){
        int longParam1=parametre1.getText().toString().trim().length();
        int longParam2=parametre2.getText().toString().trim().length();
        int longParam3=parametre3.getText().toString().trim().length();
        if(longParam1==0 || longParam2==0 || longParam3==0)
            return true;
        return false;
    }

    private double[] creationParams(){
        double param1=Double.parseDouble(parametre1.getText().toString());
        double param2=Double.parseDouble(parametre2.getText().toString());
        double param3=Double.parseDouble(parametre3.getText().toString());
        double[] lesParams={param1,param2,param3};
        return lesParams;
    }

    private void setupVariables(){
        parametre1=(EditText)findViewById(R.id.param1GammaId);
        parametre2=(EditText)findViewById(R.id.param2GammaId);
        parametre3=(EditText)findViewById(R.id.param3GammaId);

        ec=new Ecouteur();
        btnOkGammaCustom=(ImageButton)findViewById(R.id.paramsGammaCustomBtnOk);
        btnCancelGammaCustom=(ImageButton)findViewById(R.id.paramsGammaCustomBtnCancel);

        btnOkGammaCustom.setOnClickListener(ec);
        btnCancelGammaCustom.setOnClickListener(ec);
    }
}
