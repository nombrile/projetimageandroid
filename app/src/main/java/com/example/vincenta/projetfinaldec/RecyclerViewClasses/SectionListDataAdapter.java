package com.example.vincenta.projetfinaldec.RecyclerViewClasses;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.vincenta.projetfinaldec.R;

import java.util.ArrayList;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private ArrayList<ContenantFiltreRecycler> itemsList;
    private Context mContext;

    private static RecyclerViewClickListener itemListener;

    public SectionListDataAdapter(Context context,RecyclerViewClickListener itemListener, ArrayList<ContenantFiltreRecycler> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.itemListener=itemListener;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        ContenantFiltreRecycler singleItem = itemsList.get(i);

        holder.tvTitle.setText(singleItem.getTitreFiltre());
        holder.itemImage.setImageBitmap(singleItem.getImgFiltre());

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;
        protected ImageView itemImage;

        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.recyclerTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.recyclerImage);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemListener.recyclerViewListClicked(v,getAdapterPosition(),tvTitle.getText().toString());

                }
            });


        }

    }


}