package com.example.vincenta.projetfinaldec;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by Vince on 2016-02-27.
 */
public class FiltreGamma extends Filtre {

    public FiltreGamma(double[] params, String nom) {
        super(params, nom);
    }

    @Override
    public Bitmap tranformation(Bitmap imgAModif) {
        double[] lesParams=getParams();
        int hauteur =imgAModif.getHeight();
        int largeur=imgAModif.getWidth();

        for(int i=0;i<largeur;i++){
            for(int j=0;j<hauteur;j++){
                int pixel=imgAModif.getPixel(i, j);

                int leRouge= Color.red(pixel);
                int leVert=Color.green(pixel);
                int leBleu=Color.blue(pixel);

                int nouveauRouge=(int)(lesParams[0]+(255.0*lesParams[1]* Math.pow(((double)leRouge/255.0),lesParams[2])));
                int nouveauVert=(int)(lesParams[0]+(255.0*lesParams[1]* Math.pow(((double)leVert/255.0),lesParams[2])));
                int nouveauBleu=(int)(lesParams[0]+(255.0*lesParams[1]* Math.pow(((double)leBleu/255.0),lesParams[2])));

                int[] lesnvellesCouls=verifCouleur(nouveauRouge,nouveauVert,nouveauBleu);
                int nvelleCouleur=Color.argb(255, lesnvellesCouls[0], lesnvellesCouls[1], lesnvellesCouls[2]);

                imgAModif.setPixel(i,j,nvelleCouleur);
            }
        }
        return imgAModif;
    }

}
