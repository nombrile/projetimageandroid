package com.example.vincenta.projetfinaldec;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.vincenta.projetfinaldec.outilsTransformation.OutilsExternes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class AccueilActivity extends AppCompatActivity {

    ImageButton btnBibliotheque;
    ImageButton btnAppareil;
    ImageButton btnSuivant;
    ImageView imgChoisie;
    Ecouteur ec;

    OutilsExternes outilsExternes;

    String selectedImagePath;

    static final int REQUEST_CODE_CAMERA=111;
    static final int REQUEST_CODE_BIBLIO=222;
    static final int PIC_CROP_REQUEST=333;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);

        ec=new Ecouteur();

        btnBibliotheque=(ImageButton)findViewById(R.id.btnBiblio);
        btnAppareil=(ImageButton)findViewById(R.id.btnAppareil);
        btnSuivant=(ImageButton)findViewById(R.id.btnSuivant);

        imgChoisie=(ImageView)findViewById(R.id.accueilImgChoisie);


        btnAppareil.setOnClickListener(ec);
        btnBibliotheque.setOnClickListener(ec);
        btnSuivant.setOnClickListener(ec);

        outilsExternes=new OutilsExternes();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("image",((BitmapDrawable)imgChoisie.getDrawable()).getBitmap());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        imgChoisie.setImageBitmap((Bitmap)savedInstanceState.getParcelable("image"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_accueil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            if(v==btnAppareil){

                Intent i= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(i,REQUEST_CODE_CAMERA);
                File file = new File(Environment.getExternalStorageDirectory()+ File.separator + "image.jpg");
                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(i,REQUEST_CODE_CAMERA);

            }
            else if(v==btnBibliotheque){
                Intent i=new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(i,REQUEST_CODE_BIBLIO);
            }
            else if(v==btnSuivant){
                if(imgChoisie.getDrawable()==null)
                    Toast.makeText(getApplicationContext(),"Veuillez choisir une photo",Toast.LENGTH_LONG).show();
                else{
                    Intent i= new Intent(AccueilActivity.this,EditingActivity.class);
                    Bitmap imgAEditee=((BitmapDrawable)imgChoisie.getDrawable()).getBitmap();
                    i.putExtra("imagebyte",outilsExternes.imageCompress(imgAEditee));

                    startActivity(i);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("aaa", "Dans activity result");
        if(resultCode==RESULT_OK){
            Log.e("aaa","Dans result ok");
            if(requestCode==REQUEST_CODE_CAMERA){
                if(data!=null){
                    //*Bitmap photo=(Bitmap)data.getExtras().get("data");*/
                   /* //imgChoisie.setImageBitmap(photo);*/
                    //Get our saved file into a bitmap object:
                    File file = new File(Environment.getExternalStorageDirectory()+File.separator + "image.jpg");
                    Bitmap photo=outilsExternes.decodeSampledBitmapFromFile(file.getAbsolutePath(), 1000, 700);
                    photo=outilsExternes.scaleBitmap(photo, 275, AccueilActivity.this);
                    imgChoisie.setImageBitmap(photo);
                }
            }

            if(requestCode==REQUEST_CODE_BIBLIO){
                if(data!=null){
                    Uri selectedImage = data.getData();
                    try {
                        Bitmap img = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        img=outilsExternes.rotationImage(img, selectedImagePath);
                        img=outilsExternes.scaleBitmap(img, 275, AccueilActivity.this);
                        imgChoisie.setImageBitmap(img);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }



}
