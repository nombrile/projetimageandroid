package com.example.vincenta.projetfinaldec.lesFragmenta;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.vincenta.projetfinaldec.EditingActivity;
import com.example.vincenta.projetfinaldec.R;
import com.example.vincenta.projetfinaldec.outilsTransformation.SauvegardeImageAvantModif;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentSousMenuOptionsAjust.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentSousMenuOptionsAjust#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSousMenuOptionsAjust extends Fragment {


    ImageButton btnContrast,btnSaturation, btnHue, btnLuminosite;
    View view;
    Ecouteur ec;

    private OnFragmentInteractionListener mListener;

    public FragmentSousMenuOptionsAjust() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentSousMenuOptionsAjust newInstance(String param1, String param2) {
        FragmentSousMenuOptionsAjust fragment = new FragmentSousMenuOptionsAjust();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_sous_menu_options_ajust, container, false);

        btnContrast=(ImageButton)view.findViewById(R.id.ajustBtnContrast);
        btnHue=(ImageButton)view.findViewById(R.id.ajustBtnHue);
        btnSaturation=(ImageButton)view.findViewById(R.id.ajustBtnSaturation);
        btnLuminosite=(ImageButton)view.findViewById(R.id.ajustBtnLuminosite);

        ec=new Ecouteur();

        btnContrast.setOnClickListener(ec);
        btnSaturation.setOnClickListener(ec);
        btnLuminosite.setOnClickListener(ec);
        btnHue.setOnClickListener(ec);

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            try{
                ((SauvegardeImageAvantModif)getActivity()).btnFragmentMenuChoixOutils(v);
                ((EditingActivity)getActivity()).getSupportActionBar().hide();
            }
            catch (ClassCastException cce){

            }
            ((EditingActivity)getActivity()).modifierLesMenus(new FragmentSeekBar(), new FragmentBoutonsAjustements());
        }
    }

}
