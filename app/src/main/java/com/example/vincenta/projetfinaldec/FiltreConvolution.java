package com.example.vincenta.projetfinaldec;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.ArrayList;

/**
 * Created by Vince on 2016-02-27.
 */
public class FiltreConvolution extends Filtre {

    private int taille;

    public FiltreConvolution(double[] params, String nom) {
        super(params, nom);
        this.taille=params.length;
    }

    public Bitmap tranformation(Bitmap imgAModif) {
        double[] lesParams=getParams();
        int hauteur =imgAModif.getHeight();
        int largeur=imgAModif.getWidth();

        int limite=(int)(((Math.sqrt(taille))-1)/2);

        ArrayList<Integer> lesNouveauxPixels=new ArrayList<Integer>();

        for(int i=0;i<largeur;i++){
            for(int j=0;j<hauteur;j++){

                int pixelAvtModif=imgAModif.getPixel(i,j);

                if(i>=limite && i<(largeur-limite) && j>=limite && j<(hauteur-limite)){

                    int sommeRouge=0;
                    int sommeVert=0;
                    int sommeBleu=0;

                    int paramDuMasque=0;

                    for(int k=(j-limite);k<=(j+limite);k++){
                            for(int m=(i-limite);m<=(i+limite);m++){
                            int pixelDuMasque=imgAModif.getPixel(m,k);

                            int leRouge=Color.red(pixelDuMasque);
                            int leVert=Color.green(pixelDuMasque);
                            int leBleu=Color.blue(pixelDuMasque);

                            sommeRouge+=leRouge*lesParams[paramDuMasque];
                            sommeVert+=leVert*lesParams[paramDuMasque];
                            sommeBleu+=leBleu*lesParams[paramDuMasque];
                                paramDuMasque++;
                        }
                    }

                    int[] lesnvellesCouls=verifCouleur(sommeRouge,sommeVert,sommeBleu);
                    int nvelleCouleur=Color.argb(255,lesnvellesCouls[0],lesnvellesCouls[1],lesnvellesCouls[2]);

                    lesNouveauxPixels.add(nvelleCouleur);
                }
                else
                    lesNouveauxPixels.add(pixelAvtModif);
            }
        }
        int compteur=0; //Pour passer a travers l'arraylist des nouveaux pixel
        for(int x=0;x<largeur;x++){
            for(int y=0;y<hauteur;y++){
                int unPixDeLaNewPhoto=lesNouveauxPixels.get(compteur);
                imgAModif.setPixel(x, y, unPixDeLaNewPhoto);
                compteur++;
            }
        }

        return imgAModif;
    }

    public int getTaille() {
        return taille;
    }


    public void setTaille(int taille) {
        this.taille = taille;
    }

}
