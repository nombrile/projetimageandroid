package com.example.vincenta.projetfinaldec.lesFragmenta;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.vincenta.projetfinaldec.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentBoutonsAjustements.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentBoutonsAjustements#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentBoutonsAjustements extends Fragment {

    private OnFragmentInteractionListener mListener;
    ImageButton btnOk;
    ImageButton btnCancel;
    View view;
    Ecouteur ec;

    public FragmentBoutonsAjustements() {
        // Required empty public constructor
    }


    public static FragmentBoutonsAjustements newInstance(String param1, String param2) {
        FragmentBoutonsAjustements fragment = new FragmentBoutonsAjustements();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_boutons_ajustements, container, false);
        btnOk=(ImageButton)view.findViewById(R.id.btnAjustementOk);
        btnCancel=(ImageButton)view.findViewById(R.id.btnAjustementCancel);
        ec=new Ecouteur();
        btnOk.setOnClickListener(ec);
        btnCancel.setOnClickListener(ec);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void decisionAjustement(View v);
    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            try {
                ((OnFragmentInteractionListener)getActivity()).decisionAjustement(v);
            }
            catch (ClassCastException cce){

            }

        }
    }

}
