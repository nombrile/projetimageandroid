package com.example.vincenta.projetfinaldec.lesFragmenta;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.vincenta.projetfinaldec.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentRotation.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentRotation#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRotation extends Fragment {

    private OnFragmentInteractionListener mListener;
    private ImageButton btnRotation;
    View view;
    Ecouteur ec;


    public FragmentRotation() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FragmentRotation newInstance() {
        FragmentRotation fragment = new FragmentRotation();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_rotation, container, false);
        btnRotation=(ImageButton)view.findViewById(R.id.fragRotationbtnRotation);

        ec=new Ecouteur();
        btnRotation.setOnClickListener(ec);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void rotationImg(View v);
    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            ((OnFragmentInteractionListener)getActivity()).rotationImg(v);
        }
    }

}
