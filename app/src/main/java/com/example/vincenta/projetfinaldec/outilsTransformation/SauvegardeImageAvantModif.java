package com.example.vincenta.projetfinaldec.outilsTransformation;

import android.view.View;

/**
 * Created by Vince on 2016-04-10.
 */
public interface SauvegardeImageAvantModif {
    void btnFragmentMenuChoixOutils(View v);
}
