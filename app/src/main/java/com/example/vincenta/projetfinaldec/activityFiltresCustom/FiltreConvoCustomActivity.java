package com.example.vincenta.projetfinaldec.activityFiltresCustom;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.vincenta.projetfinaldec.R;
import com.example.vincenta.projetfinaldec.lesFragmenta.FragmentMenuChoixOutils;

import java.util.ArrayList;

public class FiltreConvoCustomActivity extends AppCompatActivity {

    EditText param1,param2,param3,param4,param5,param6,param7,param8,param9,param10,param11,
            param12,param13,param14,param15,param16,param17,param18,param19,param20,param21,param22,
            param23,param24,param25;


    ImageButton btnOk,btnCancel;
    int nbParam;

    Ecouteur ec;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b=getIntent().getExtras();
        nbParam=b.getInt("nbParams");
        switch (nbParam){
            case 3: setContentView(R.layout.activity_filtre_convo_3);
                break;
            case 5: setContentView(R.layout.activity_filtre_convo_5);
                break;
        }

        setupVariables();

    }

    private void setupVariables(){
        if(nbParam==3){
            param1=(EditText)findViewById(R.id.convo3Param1);
            param2=(EditText)findViewById(R.id.convo3Param2);
            param3=(EditText)findViewById(R.id.convo3Param3);
            param4=(EditText)findViewById(R.id.convo3Param4);
            param5=(EditText)findViewById(R.id.convo3Param5);
            param6=(EditText)findViewById(R.id.convo3Param6);
            param7=(EditText)findViewById(R.id.convo3Param7);
            param8=(EditText)findViewById(R.id.convo3Param8);
            param9=(EditText)findViewById(R.id.convo3Param9);
            ec=new Ecouteur();

            btnOk=(ImageButton)findViewById(R.id.btnConvoCustom3xOk);
            btnCancel=(ImageButton)findViewById(R.id.btnConvoCustom3xCancel);
            btnOk.setOnClickListener(ec);
            btnCancel.setOnClickListener(ec);

        }
        if(nbParam==5){
            param1=(EditText)findViewById(R.id.convo5Param1);
            param2=(EditText)findViewById(R.id.convo5Param2);
            param3=(EditText)findViewById(R.id.convo5Param3);
            param4=(EditText)findViewById(R.id.convo5Param4);
            param5=(EditText)findViewById(R.id.convo5Param5);
            param6=(EditText)findViewById(R.id.convo5Param6);
            param7=(EditText)findViewById(R.id.convo5Param7);
            param8=(EditText)findViewById(R.id.convo5Param8);
            param9=(EditText)findViewById(R.id.convo5Param9);
            param10=(EditText)findViewById(R.id.convo5Param10);
            param11=(EditText)findViewById(R.id.convo5Param11);
            param12=(EditText)findViewById(R.id.convo5Param12);
            param13=(EditText)findViewById(R.id.convo5Param13);
            param14=(EditText)findViewById(R.id.convo5Param14);
            param15=(EditText)findViewById(R.id.convo5Param15);
            param16=(EditText)findViewById(R.id.convo5Param15);
            param17=(EditText)findViewById(R.id.convo5Param17);
            param18=(EditText)findViewById(R.id.convo5Param18);
            param19=(EditText)findViewById(R.id.convo5Param19);
            param20=(EditText)findViewById(R.id.convo5Param20);
            param21=(EditText)findViewById(R.id.convo5Param21);
            param22=(EditText)findViewById(R.id.convo5Param21);
            param23=(EditText)findViewById(R.id.convo5Param23);
            param24=(EditText)findViewById(R.id.convo5Param24);
            param25=(EditText)findViewById(R.id.convo5Param25);

            ec=new Ecouteur();

            btnOk=(ImageButton)findViewById(R.id.btnConvoCustom5xOk);
            btnCancel=(ImageButton)findViewById(R.id.btnConvoCustom5xCancel);
            btnOk.setOnClickListener(ec);
            btnCancel.setOnClickListener(ec);
        }

    }

    private boolean champsVide(){
        int longParam1=param1.getText().toString().trim().length();
        int longParam2=param2.getText().toString().trim().length();
        int longParam3=param3.getText().toString().trim().length();
        int longParam4=param4.getText().toString().trim().length();
        int longParam5=param5.getText().toString().trim().length();
        int longParam6=param6.getText().toString().trim().length();
        int longParam7=param7.getText().toString().trim().length();
        int longParam8=param8.getText().toString().trim().length();
        int longParam9=param9.getText().toString().trim().length();


        if(nbParam==3){
            int[] lesLongParam=new int[]{longParam1,longParam2,longParam3,longParam4,longParam5
                    ,longParam6,longParam7,longParam8,longParam9};
            if(longueurDeZero(lesLongParam))
                return true;
        }

        if(nbParam==5){
            int longParam10=param10.getText().toString().trim().length();
            int longParam11=param11.getText().toString().trim().length();
            int longParam12=param12.getText().toString().trim().length();
            int longParam13=param13.getText().toString().trim().length();
            int longParam14=param14.getText().toString().trim().length();
            int longParam15=param15.getText().toString().trim().length();
            int longParam16=param16.getText().toString().trim().length();
            int longParam17=param17.getText().toString().trim().length();
            int longParam18=param18.getText().toString().trim().length();
            int longParam19=param19.getText().toString().trim().length();
            int longParam20=param20.getText().toString().trim().length();
            int longParam21=param21.getText().toString().trim().length();
            int longParam22=param22.getText().toString().trim().length();
            int longParam23=param23.getText().toString().trim().length();
            int longParam24=param24.getText().toString().trim().length();
            int longParam25=param25.getText().toString().trim().length();


            int[] lesLongParam = new int[]{longParam1, longParam2, longParam3, longParam4, longParam5
                    , longParam6, longParam7, longParam8, longParam9,longParam10,longParam11,longParam12,
                    longParam13,longParam14,longParam15,longParam16,longParam17,longParam18,longParam19,
                    longParam20,longParam21,longParam22,longParam23,longParam24,longParam25};
            if (longueurDeZero(lesLongParam))
                return true;

        }

        return false;
    }

    private double[] creationParams(){
        double parametre1=Double.parseDouble(param1.getText().toString());
        double parametre2=Double.parseDouble(param2.getText().toString());
        double parametre3=Double.parseDouble(param3.getText().toString());
        double parametre4=Double.parseDouble(param4.getText().toString());
        double parametre5=Double.parseDouble(param5.getText().toString());
        double parametre6=Double.parseDouble(param6.getText().toString());
        double parametre7=Double.parseDouble(param7.getText().toString());
        double parametre8=Double.parseDouble(param8.getText().toString());
        double parametre9=Double.parseDouble(param9.getText().toString());

        if( nbParam==3){
            double[] lesParams=new double[]{parametre1,parametre2,parametre3,parametre4,parametre5,
                    parametre6,parametre7,parametre8,parametre9};
            return lesParams;
        }

        if( nbParam==5){
            double parametre10=Double.parseDouble(param10.getText().toString());
            double parametre11=Double.parseDouble(param11.getText().toString());
            double parametre12=Double.parseDouble(param12.getText().toString());
            double parametre13=Double.parseDouble(param13.getText().toString());
            double parametre14=Double.parseDouble(param14.getText().toString());
            double parametre15=Double.parseDouble(param15.getText().toString());
            double parametre16=Double.parseDouble(param16.getText().toString());
            double parametre17=Double.parseDouble(param17.getText().toString());
            double parametre18=Double.parseDouble(param18.getText().toString());
            double parametre19=Double.parseDouble(param19.getText().toString());
            double parametre20=Double.parseDouble(param20.getText().toString());
            double parametre21=Double.parseDouble(param21.getText().toString());
            double parametre22=Double.parseDouble(param22.getText().toString());
            double parametre23=Double.parseDouble(param23.getText().toString());
            double parametre24=Double.parseDouble(param24.getText().toString());
            double parametre25=Double.parseDouble(param25.getText().toString());

            double[] lesParams=new double[]{parametre1,parametre2,parametre3,parametre4,parametre5,
                    parametre6,parametre7,parametre8,parametre9,parametre10,parametre11,parametre12,
            parametre13,parametre14,parametre15,parametre16,parametre17,parametre18,parametre19,
            parametre20,parametre21,parametre22,parametre23,parametre24,parametre25};
            return lesParams;
        }
        return null;
    }

    private boolean longueurDeZero(int[] lesParamAVerif){
        for(int i=0;i<lesParamAVerif.length;i++)
            if(lesParamAVerif[i]==0)
                return true;
        return false;
    }
   private class Ecouteur implements View.OnClickListener{

       @Override
       public void onClick(View v) {
           if(v==btnOk){
               if(champsVide()){
                   Toast.makeText(getApplicationContext(), "Remplissez tous les champs", Toast.LENGTH_LONG).show();
               }
               else{
                   double[] lesParams=creationParams();
                   Intent i=new Intent();
                   i.putExtra("lesParams",lesParams);
                   i.putExtra("choix","ok");
                   setResult(RESULT_OK, i);
                   finish();
               }
           }

           if(v==btnCancel){
               Intent i=new Intent();
               i.putExtra("choix","cancel");
               setResult(RESULT_OK,i);
               finish();
            }

       }
   }
}
