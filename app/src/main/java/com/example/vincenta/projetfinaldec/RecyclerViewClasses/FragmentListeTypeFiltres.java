package com.example.vincenta.projetfinaldec.RecyclerViewClasses;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.vincenta.projetfinaldec.EditingActivity;
import com.example.vincenta.projetfinaldec.lesFragmenta.FragmentBoutonsAjustements;
import com.example.vincenta.projetfinaldec.R;
import com.example.vincenta.projetfinaldec.outilsTransformation.SauvegardeImageAvantModif;


public class FragmentListeTypeFiltres extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    Button btnLineaire;
    Button btnGamma;
    Button btnConvolution;
    View view;
    Ecouteur ecout;
    private OnFragmentInteractionListener mListener;


    public FragmentListeTypeFiltres() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentListeTypeFiltres newInstance() {
        FragmentListeTypeFiltres fragment = new FragmentListeTypeFiltres();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_liste_type_filtres, container, false);
        btnLineaire=(Button)view.findViewById(R.id.fragmentTypeFiltreBtnLineaire);
        btnGamma=(Button)view.findViewById(R.id.fragmentTypeFiltreBtnGamma);
        btnConvolution=(Button)view.findViewById(R.id.fragmentTypeFiltreBtnConvolution);
        // Inflate the layout for this fragment
        ecout=new Ecouteur();
        btnLineaire.setOnClickListener(ecout);
        btnGamma.setOnClickListener(ecout);
        btnConvolution.setOnClickListener(ecout);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            try{
                ((SauvegardeImageAvantModif)getActivity()).btnFragmentMenuChoixOutils(v);
            }
            catch (ClassCastException cce){

            }
            if(v==btnLineaire)
                ((EditingActivity)getActivity()).choixTypeFiltreAfficher("filtresLineaires");

            else if(v==btnGamma)
                ((EditingActivity)getActivity()).choixTypeFiltreAfficher("filtresGamma");

            else
                ((EditingActivity)getActivity()).choixTypeFiltreAfficher("filtresConvolution");
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void choixTypeFiltreAfficher(String typeFiltre);
    }

}
