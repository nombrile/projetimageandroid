package com.example.vincenta.projetfinaldec.outilsTransformation;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by Vince on 2016-03-26.
 */
public class HistoriqueImage {

    public static ArrayList<Bitmap> lesImagesEditees=new ArrayList<>();

    public static void ajouterImgHistorique(Bitmap img){
        lesImagesEditees.add(img);
    }

    public static void retirerImgHistorque(){
        lesImagesEditees.remove(lesImagesEditees.size()-1);
    }


    public static Bitmap obtenirImgHistorique(){
        return lesImagesEditees.get(lesImagesEditees.size()-1);
    }

    public static Bitmap revertImgHistorique(){
        if(lesImagesEditees.size()>1) {
            retirerImgHistorque();
        }
        return obtenirImgHistorique();
    }

    public static Bitmap restartHistorique(){
        while(lesImagesEditees.size()>1)
            retirerImgHistorque();
        return obtenirImgHistorique();
    }
}
