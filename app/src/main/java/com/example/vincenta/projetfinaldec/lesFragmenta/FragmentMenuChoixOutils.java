package com.example.vincenta.projetfinaldec.lesFragmenta;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.vincenta.projetfinaldec.EditingActivity;
import com.example.vincenta.projetfinaldec.R;
import com.example.vincenta.projetfinaldec.RecyclerViewClasses.FragmentListeTypeFiltres;
import com.example.vincenta.projetfinaldec.outilsTransformation.SauvegardeImageAvantModif;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMenuChoixOutils.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMenuChoixOutils#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMenuChoixOutils extends Fragment {

    ImageButton btnFiltres, btnAjustement,btnRotation;
    Ecouteur ec;
    View view;

    private OnFragmentInteractionListener mListener;

    public FragmentMenuChoixOutils() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FragmentMenuChoixOutils newInstance(String param1, String param2) {
        FragmentMenuChoixOutils fragment = new FragmentMenuChoixOutils();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_menu_outils, container, false);
        btnFiltres=(ImageButton)view.findViewById(R.id.menOutilsBtnFiltres);
        btnAjustement=(ImageButton)view.findViewById(R.id.menOutilsBtnAjustable);
        btnRotation=(ImageButton)view.findViewById(R.id.menOutilsBtnRotation);

        ec=new Ecouteur();
        btnFiltres.setOnClickListener(ec);
        btnAjustement.setOnClickListener(ec);
        btnRotation.setOnClickListener(ec);

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            try{
                ((SauvegardeImageAvantModif)getActivity()).btnFragmentMenuChoixOutils(v);
            }
            catch (ClassCastException cce){

            }
            if(v==btnFiltres){
                ((EditingActivity)getActivity()).modifierLesMenus(new FragmentMenuChoixOutils(),new FragmentListeTypeFiltres());
            }
            if(v==btnAjustement){
                ((EditingActivity)getActivity()).modifierLesMenus(new FragmentMenuChoixOutils(), new FragmentSousMenuOptionsAjust());
            }
            if(v==btnRotation) {
                ((EditingActivity)getActivity()).modifierLesMenus(new FragmentRotation(),new FragmentBoutonsAjustements());
                ((EditingActivity)getActivity()).getSupportActionBar().hide();

            }

        }
    }

}
