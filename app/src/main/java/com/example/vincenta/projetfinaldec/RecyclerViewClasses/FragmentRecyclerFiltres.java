package com.example.vincenta.projetfinaldec.RecyclerViewClasses;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.vincenta.projetfinaldec.EditingActivity;
import com.example.vincenta.projetfinaldec.Filtre;
import com.example.vincenta.projetfinaldec.FiltreConvolution;
import com.example.vincenta.projetfinaldec.FiltreGamma;
import com.example.vincenta.projetfinaldec.FiltreLineaire;
import com.example.vincenta.projetfinaldec.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentRecyclerFiltres.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentRecyclerFiltres#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRecyclerFiltres extends Fragment implements RecyclerViewClickListener {


    View view;
    ArrayList<ContenantFiltreRecycler> itemsFiltres;
    String typeFiltre;
    Bitmap thumbnail;

    private OnFragmentInteractionListener mListener;

    public FragmentRecyclerFiltres() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment FragmentRecyclerFiltres.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRecyclerFiltres newInstance(String typeFiltre, Bitmap thumbnail) {
        FragmentRecyclerFiltres fragment = new FragmentRecyclerFiltres();
        Bundle args = new Bundle();
        args.putString("type",typeFiltre);
        args.putParcelable("thumbnail",thumbnail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =inflater.inflate(R.layout.fragment_recycler_filtres, container, false);
        RecyclerView recyclerdeFiltre=(RecyclerView)view.findViewById(R.id.recyclerFragmentFiltres);
        itemsFiltres=new ArrayList<>();

        typeFiltre=getArguments().getString("type");
        thumbnail=getArguments().getParcelable("thumbnail");

        remplirRecycler(typeFiltre);
        recyclerdeFiltre.setHasFixedSize(true);

        SectionListDataAdapter adapter =new SectionListDataAdapter(getActivity().getApplicationContext(),this,itemsFiltres);
        recyclerdeFiltre.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        recyclerdeFiltre.setAdapter(adapter);



        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void remplirRecycler( String typeFiltre){
        ContenantFiltreRecycler unItem;

        if(typeFiltre.equals("filtresLineaires")) {
            HashMap<String, FiltreLineaire> lesFiltresLin = ((EditingActivity) getActivity()).getLesFiltresPredef().get("filtresLineaires");
            for (String key : lesFiltresLin.keySet()) {
                unItem = new ContenantFiltreRecycler();
                unItem.setTitreFiltre(key);
                FiltreLineaire unfiltre = lesFiltresLin.get(key);
                Bitmap imgModifiable = thumbnail.copy(thumbnail.getConfig(), true);
                Bitmap imgCarte = unfiltre.tranformation(imgModifiable);
                unItem.setImgFiltre(imgCarte);
                itemsFiltres.add(unItem);
            }
        }

        else if(typeFiltre.equals("filtresGamma")) {
            HashMap<String, FiltreGamma> lesFiltresGam = ((EditingActivity) getActivity()).getLesFiltresPredef().get("filtresGamma");
            for (String key : lesFiltresGam.keySet()) {
                unItem = new ContenantFiltreRecycler();
                unItem.setTitreFiltre(key);
                FiltreGamma unfiltre = lesFiltresGam.get(key);
                Bitmap imgModifiable = thumbnail.copy(thumbnail.getConfig(), true);
                Bitmap imgCarte = unfiltre.tranformation(imgModifiable);
                unItem.setImgFiltre(imgCarte);
                itemsFiltres.add(unItem);
            }
        }

        else if(typeFiltre.equals("filtresConvolution")) {
            HashMap<String, FiltreConvolution> lesFiltresConv = ((EditingActivity) getActivity()).getLesFiltresPredef().get("filtresConvolution");
            for (String key : lesFiltresConv.keySet()) {
                unItem = new ContenantFiltreRecycler();
                unItem.setTitreFiltre(key);
                FiltreConvolution unfiltre = lesFiltresConv.get(key);
                Bitmap imgModifiable = thumbnail.copy(thumbnail.getConfig(), true);
                Bitmap imgCarte = unfiltre.tranformation(imgModifiable);
                unItem.setImgFiltre(imgCarte);
                itemsFiltres.add(unItem);
            }
        }

        unItem = new ContenantFiltreRecycler();
        unItem.setTitreFiltre("Custom");
        Bitmap imgCustom = BitmapFactory.decodeResource(getResources(), R.drawable.edit);
        unItem.setImgFiltre(imgCustom);
        itemsFiltres.add(unItem);


    }

    @Override
    public void recyclerViewListClicked(View v, int position, String nomFiltre) {
        ((EditingActivity)getActivity()).applicationFiltre(nomFiltre, typeFiltre);
    }


    public interface OnFragmentInteractionListener {
        void applicationFiltre(String nomFiltre, String typeFiltre);
    }


}
