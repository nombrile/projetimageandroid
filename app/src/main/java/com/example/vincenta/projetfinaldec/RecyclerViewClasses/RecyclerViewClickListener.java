package com.example.vincenta.projetfinaldec.RecyclerViewClasses;

import android.view.View;

/**
 * Created by Vince on 2016-04-17.
 */
public interface RecyclerViewClickListener
{
    public void recyclerViewListClicked(View v, int position, String nomFiltre);
}