package com.example.vincenta.projetfinaldec.outilsTransformation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.ByteArrayOutputStream;

/**
 * Created by Vince on 2016-05-15.
 */
public class OutilsExternes {



    public Bitmap rotationImage(Bitmap photoARotater,String pathImgARotate){
        try {
            ExifInterface exif = new ExifInterface(pathImgARotate);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Matrix matrix = new Matrix();
            if (orientation ==ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.postRotate(90);
            }
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.postRotate(180);
            }
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.postRotate(270);
            }
            photoARotater = Bitmap.createBitmap(photoARotater, 0, 0, photoARotater.getWidth(), photoARotater.getHeight(), matrix, true); // rotating bitmap
        }
        catch (Exception e) {

        }
        return photoARotater;
    }



    public Bitmap scaleBitmap(Bitmap photo, int newHeight, Context context) {

        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        int h;
        int w;
        if(photo.getHeight()>photo.getWidth()) {
            h = (int) (newHeight * densityMultiplier);
            w = (int) (h * photo.getWidth() / ((double) photo.getHeight()));

            photo = Bitmap.createScaledBitmap(photo, w, h, true);
        }

        else if(photo.getHeight()<photo.getWidth()) {
            w = (int) (newHeight * densityMultiplier);
            h = (int) (w * photo.getHeight() / ((double) photo.getWidth()));
            photo = Bitmap.createScaledBitmap(photo, w, h, true);
        }

        else{
            h = (int) (newHeight * densityMultiplier);
            w = (int) (newHeight * densityMultiplier);
        }
        photo = Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }


    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight)
    { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight)
        {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth)
        {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }


    public byte[] imageCompress(Bitmap image){

        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 50, bs);
        return bs.toByteArray();
    }

    public Bitmap decompressImage(byte[] arrayImage){
        Bitmap image = BitmapFactory.decodeByteArray(
                arrayImage, 0, arrayImage.length);
        return image;
    }
}
