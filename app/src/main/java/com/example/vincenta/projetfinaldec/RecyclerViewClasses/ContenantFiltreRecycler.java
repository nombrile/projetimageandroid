package com.example.vincenta.projetfinaldec.RecyclerViewClasses;

import android.graphics.Bitmap;


/**
 * Created by Vince on 2016-04-05.
 */
public class ContenantFiltreRecycler  {

    private Bitmap imgFiltre;
    private String titreFiltre;

    public ContenantFiltreRecycler(){}


    public ContenantFiltreRecycler(Bitmap img, String nomFiltre) {
        this.imgFiltre=img;
        this.titreFiltre=nomFiltre;
    }

    public Bitmap getImgFiltre() {
        return imgFiltre;
    }

    public void setImgFiltre(Bitmap imgFiltre) {
        this.imgFiltre = imgFiltre;
    }

    public String getTitreFiltre() {
        return titreFiltre;
    }

    public void setTitreFiltre(String titreFiltre) {
        this.titreFiltre = titreFiltre;
    }
}
