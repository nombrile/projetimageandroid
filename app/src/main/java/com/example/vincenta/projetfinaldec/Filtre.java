package com.example.vincenta.projetfinaldec;

import android.graphics.Bitmap;

/**
 * Created by Vince on 2016-02-27.
 * Bitmap tgtImg = BitmapFactory.decodeFile("ImageD2.jpg");
 */
public abstract class Filtre {

    private String nom;
    private double[] params;

    public Filtre(double[] params, String nom) {
        this.params = params;
        this.nom = nom;
    }

    public abstract Bitmap tranformation(Bitmap imgAModif);

    public int[] verifCouleur(int rouge, int vert, int bleu){
        if(rouge<0)
            rouge = 0;
        if(rouge>255)
            rouge=255;
        if(vert<0)
            vert = 0;
        if(vert>255)
            vert=255;
        if(bleu<0)
            bleu = 0;
        if(bleu>255)
            bleu=255;

        return new int[]{rouge, vert, bleu};

    }

    public double[] getParams() {
        return params;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setParams(double[] params) {
        this.params = params;
    }
}
