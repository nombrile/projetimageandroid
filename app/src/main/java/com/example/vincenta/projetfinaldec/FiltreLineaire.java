package com.example.vincenta.projetfinaldec;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by Vince on 2016-02-27.
 */
public class FiltreLineaire extends Filtre {

    public FiltreLineaire(double[] params, String nom) {
        super(params, nom);
    }

    @Override
    public Bitmap tranformation(Bitmap imgAModif) {
        double[] lesParams=getParams();
        int hauteur =imgAModif.getHeight();
        int largeur=imgAModif.getWidth();

        for(int i=0;i<largeur;i++){
            for(int j=0;j<hauteur;j++){
                int pixel=imgAModif.getPixel(i, j);

                int leRouge=Color.red(pixel);
                int leVert=Color.green(pixel);
                int leBleu=Color.blue(pixel);

                int nouveauRouge=(int)(lesParams[0]+(lesParams[1]*(double)leRouge)+(lesParams[2]*(double)leVert)+(lesParams[3]*(double)leBleu));
                int nouveauVert=(int)(lesParams[4]+(lesParams[5]*(double)leRouge)+(lesParams[6]*(double)leVert)+(lesParams[7]*(double)leBleu));
                int nouveauBleu=(int)(lesParams[8]+(lesParams[9]*(double)leRouge)+(lesParams[10]*(double)leVert)+(lesParams[11]*(double)leBleu));

                int[] lesnvellesCouls=verifCouleur(nouveauRouge,nouveauVert,nouveauBleu);
                int nvelleCouleur=Color.argb(255,lesnvellesCouls[0],lesnvellesCouls[1],lesnvellesCouls[2]);

                imgAModif.setPixel(i,j,nvelleCouleur);


            }
        }
        return imgAModif;
    }

}
