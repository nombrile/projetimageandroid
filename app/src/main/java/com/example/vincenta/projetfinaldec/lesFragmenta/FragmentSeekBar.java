package com.example.vincenta.projetfinaldec.lesFragmenta;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.example.vincenta.projetfinaldec.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentSeekBar.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentSeekBar#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSeekBar extends Fragment {
    //
    private OnFragmentInteractionListener mListener;

    SeekBar laSeekBar;
    View view;
    Ecouteur ec;

    public FragmentSeekBar() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FragmentSeekBar newInstance(String param1, String param2) {
        FragmentSeekBar fragment = new FragmentSeekBar();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_seek_bar, container, false);
        laSeekBar=(SeekBar)view.findViewById(R.id.fragmentSeekBar);
        laSeekBar.setMax(0);
        laSeekBar.setMax(200);
        laSeekBar.setProgress(100);

        ec=new Ecouteur();
        laSeekBar.setOnSeekBarChangeListener(ec);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onProgressChange(int progress);
    }

    private  class Ecouteur implements SeekBar.OnSeekBarChangeListener{

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            try{
                ((OnFragmentInteractionListener)getActivity()).onProgressChange(seekBar.getProgress()-100);
            }
            catch (ClassCastException cce){
                Log.e("errseekbar",cce.toString());
            }
        }

    }


}
