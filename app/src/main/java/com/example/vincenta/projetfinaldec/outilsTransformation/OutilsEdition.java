package com.example.vincenta.projetfinaldec.outilsTransformation;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.example.vincenta.projetfinaldec.FiltreConvolution;
import com.example.vincenta.projetfinaldec.FiltreGamma;
import com.example.vincenta.projetfinaldec.FiltreLineaire;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Vince on 2016-02-28.
 */
public class OutilsEdition {


 //Fonction pour changer la teinte, la saturation et la luminosité d'une image

    public Bitmap chgHslImage(Bitmap imgAchanger, double intensite, int paramAChanger){
        List<List<Double>> lesHsl;
        lesHsl=rgbToHSL(imgAchanger);
        Bitmap nvelleImage;

        for(int i=0;i<lesHsl.size();i++) {
            if(paramAChanger==0) {
                double teinte = lesHsl.get(i).get(0);
                teinte += intensite;
                if(teinte<0.0)
                    teinte=0.0;
                if(teinte>1.0)
                    teinte=1;
                lesHsl.get(i).set(0,teinte);
            }
            else if(paramAChanger==1) {
                double saturation = lesHsl.get(i).get(1);
                saturation += intensite;
                if(saturation<0.0)
                    saturation=0.0;
                if(saturation>1.0)
                    saturation=1;
                lesHsl.get(i).set(1,saturation);
            }
            else if(paramAChanger==2) {
                double luminosite = lesHsl.get(i).get(2);
                luminosite += intensite;
                if(luminosite<0.0)
                    luminosite=0.0;
                if(luminosite>1.0)
                    luminosite=1;
                lesHsl.get(i).set(2,luminosite);
            }
        }
        nvelleImage=hslToRGB(lesHsl, imgAchanger);
        return nvelleImage;
    }

    //Pour changer les couleur de la photo de RGB à HSL
    public List<List<Double>> rgbToHSL(Bitmap imgAModif){

        int hauteur =imgAModif.getHeight();
        int largeur=imgAModif.getWidth();

        List<List<Double>> lesHsl = new ArrayList<List<Double>>();

        for(int i=0;i<largeur;i++){
            for(int j=0;j<hauteur;j++){
                int lePixel=imgAModif.getPixel(i,j);

                double var_R = ( Color.red(lePixel) / 255.0 );
                double var_G = ( Color.green(lePixel) / 255.0 );
                double var_B = ( Color.blue(lePixel)/ 255.0 );

                double L=0.0;
                double H=0.0;
                double S=0.0;

                double var_Min = Math.min(var_R, Math.min(var_G, var_B) );    //Min. value of RGB
                double var_Max = Math.max(var_R, Math.max(var_G, var_B));    //Max. value of RGB
                double del_Max = var_Max - var_Min;             //Delta RGB value

                L = ( var_Max + var_Min ) / 2.0;

                if ( del_Max == 0.0 )                     //This is a gray, no chroma...
                {
                    H = 0.0;                                //HSL results from 0 to 1
                    S = 0.0;
                }
                else                                    //Chromatic data...
                {
                    if ( L < 0.5 )
                        S = del_Max / ( var_Max + var_Min );
                    else
                        S = del_Max / ( 2.0 - var_Max - var_Min );

                    double del_R = ( ( ( var_Max - var_R ) / 6.0 ) + ( del_Max / 2.0 ) ) / del_Max;
                    double del_G = ( ( ( var_Max - var_G ) / 6.0 ) + ( del_Max / 2.0 ) ) / del_Max;
                    double del_B = ( ( ( var_Max - var_B ) / 6.0 ) + ( del_Max / 2.0 ) ) / del_Max;

                    if  ( var_R == var_Max )
                        H = del_B - del_G;
                    else if ( var_G == var_Max )
                        H = ( 1.0 / 3.0 ) + del_R - del_B;
                    else if ( var_B == var_Max )
                        H = ( 2.0 / 3.0 ) + del_G - del_R;

                    if ( H < 0.0 )
                        H += 1.0;
                    if ( H > 1.0 )
                        H -= 1.0;


                }
                ArrayList leHSL=new ArrayList<Double>();
                leHSL.add(H);
                leHSL.add(S);
                leHSL.add(L);

                lesHsl.add(leHSL);
            }
        }
        return lesHsl;

    }

    //Pour passer de HSL à RGB pour chaque pixel de l'image
    public Bitmap hslToRGB(List<List<Double>> lesHsl, Bitmap imgARetourner){

        int compteHSL=0;

        double H;
        double S;
        double L;

        int R;
        int G;
        int B;

        double var_1;
        double var_2;

        int largeur=imgARetourner.getWidth();
        int hauteur=imgARetourner.getHeight();


        for(int i=0;i<largeur;i++) {
            for (int j = 0; j < hauteur; j++) {
                H=lesHsl.get(compteHSL).get(0);
                S=lesHsl.get(compteHSL).get(1);
                L=lesHsl.get(compteHSL).get(2);

                if ( S == 0.0 )                       //HSL from 0 to 1
                {
                    R =(int) (L * 255.0);                      //RGB results from 0 to 255
                    G =(int) (L * 255.0);
                    B =(int) (L * 255.0);
                }
                else
                {
                    if ( L < 0.5 )
                        var_2 = L * ( 1.0 + S );
                    else
                        var_2 = ( L + S ) - ( S * L );

                    var_1 = 2.0 * L - var_2;

                    R = (int)(255 * Hue_2_RGB( var_1, var_2, H + ( 1.0 / 3.0 ) ));
                    G =(int) (255 * Hue_2_RGB( var_1, var_2, H ));
                    B =(int) (255 * Hue_2_RGB( var_1, var_2, H - ( 1.0 / 3.0 ) ));
                }
                int[] lesnvellesCouls=verifCouleur(R, G, B);
                int nvelleCouleur=Color.argb(255,lesnvellesCouls[0],lesnvellesCouls[1],lesnvellesCouls[2]);
                imgARetourner.setPixel(i, j, nvelleCouleur);

                compteHSL++;
            }
        }
        return imgARetourner;
    }

    public double Hue_2_RGB( double v1, double v2, double vH )             //Function Hue_2_RGB
    {
        if ( vH < 0.0 )
            vH += 1.0;
        if ( vH > 1.0 )
            vH -= 1.0;
        if ( ( 6.0 * vH ) < 1.0 )
            return ( v1 + ( v2 - v1 ) * 6.0 * vH );
        if ( ( 2.0 * vH ) < 1.0 )
            return ( v2 );
        if ( ( 3.0 * vH ) < 2.0 )
            return ( v1 + ( v2 - v1 ) * ( ( 2.0 / 3.0 ) - vH ) * 6.0 );
        return ( v1 );
    }

    //Pour ajuster le contraste de l'image
    public Bitmap adjustContrast(Bitmap img, double contrast){
        int hauteur=img.getHeight();
        int largeur =img.getWidth();

        double factor = (259.0 * (contrast + 255.0)) / (255.0 * (259.0 - contrast));
        System.out.println("largeur: "+largeur+", hauteur: "+hauteur);
        for(int i=0;i<largeur;i++){
            for(int j=0;j<hauteur;j++){
                int lePixel=img.getPixel(i,j);

                double var_R =  Color.red(lePixel);
                double var_G =  Color.green(lePixel);
                double var_B =  Color.blue(lePixel);

                double r2=factor*(var_R-128.0)+128.0;

                double g2=factor*(var_G-128.0)+128.0;

                double b2=factor*(var_B-128.0)+128.0;

                int[] lesnvellesCouls=verifCouleur((int)r2, (int)g2, (int)b2);
                int nvelleCouleur=Color.argb(255,lesnvellesCouls[0],lesnvellesCouls[1],lesnvellesCouls[2]);

                img.setPixel(i, j, nvelleCouleur);
            }
        }
        return img;
    }

    //Pour faire rotation de l'image
    public Bitmap rotationImg(Bitmap img){
        int hauteur=img.getHeight();
        int largeur =img.getWidth();
        ArrayList<Integer> lesPixels=new ArrayList<>();
        for(int i=0;i<largeur;i++){
            for(int j=0;j<hauteur;j++){
                int lePixel=img.getPixel(i,j);
                lesPixels.add(lePixel);
            }
        }
            //j=i et i=hauteur-j
            Bitmap imgAReturn = Bitmap.createBitmap(hauteur, largeur, Bitmap.Config.RGB_565);
            int indexPix = 0;
            for (int i = 0; i < largeur; i++) {
                for (int j = hauteur - 1; j >= 0; j--) {
                    imgAReturn.setPixel(j, i, lesPixels.get(indexPix));
                    indexPix++;
                }
            }
            img=imgAReturn;

        return img;
    }


    //Pour verifier que le RGB soit bien entre 0 et 255
    public int[] verifCouleur(int rouge, int vert, int bleu){
        if(rouge<0)
            rouge = 0;
        if(rouge>255)
            rouge=255;
        if(vert<0)
            vert = 0;
        if(vert>255)
            vert=255;
        if(bleu<0)
            bleu = 0;
        if(bleu>255)
            bleu=255;

        return new int[]{rouge, vert, bleu};

    }

//Création des filtres linéaires prédéfinis
    public HashMap<String,FiltreLineaire> creationFiltreLinPredef(){
        HashMap<String,FiltreLineaire> lesFiltres=new HashMap<String,FiltreLineaire>();

        FiltreLineaire filtrePredf=new FiltreLineaire(new double[]{255,-0.333,-0.333,-0.333,255,-0.333,-0.333,-0.333,255,-0.333,-0.333,-0.333},"negatifMono");
        lesFiltres.put("negatifMono",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{0,1,0,0,0,1,0,0,0,1,0,0},"extraRouge");
        lesFiltres.put("extraRouge",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0,1,0,0,0,1,0,0,0,1,0},"extraVert");
        lesFiltres.put("extraVert",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0,0,1,0,0,0,1,0,0,0,1},"extraBleu");
        lesFiltres.put("extraBleu",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1,1,0,0,1,1,0,0,1,1,0},"extraJaune");
        lesFiltres.put("extraJaune",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0,1,1,0,0,1,1,0,0,1,1},"extraCyan");
        lesFiltres.put("extraCyan",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1,0,1,0,1,0,1,0,1,0,1},"extraMagenta");
        lesFiltres.put("extraMagenta",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{0,0.333,0.333,0.333,0,0.333,0.333,0.333,0,0.333,0.333,0.333},"moyArithm");
        lesFiltres.put("moyArithm",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0.30,0.59,0.11,0,0.30,0.59,0.11,0,0.30,0.59,0.11},"moyAnthropo");
        lesFiltres.put("moyAnthropo",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0.21,0.71,0.08,0,0.21,0.71,0.08,0,0.21,0.71,0.08},"moyLumino");
        lesFiltres.put("moyLumino",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{32,0.333,0.333,0.333,32,0.333,0.333,0.333,32,0.333,0.333,0.333},"RehauAbs12");
        lesFiltres.put("RehauAbs12",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{64,0.333,0.333,0.333,64,0.333,0.333,0.333,64,0.333,0.333,0.333},"RehauAbs25");
        lesFiltres.put("RehauAbs25",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{-32,0.333,0.333,0.333,-32,0.333,0.333,0.333,-32,0.333,0.333,0.333},"AssombAbs12");
        lesFiltres.put("AssombAbs12",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{-64,0.333,0.333,0.333,-64,0.333,0.333,0.333,-64,0.333,0.333,0.333},"AssombAbs25");
        lesFiltres.put("AssombAbs25",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{0,0.375,0.375,0.375,0,0.375,0.375,0.375,0,0.375,0.375,0.375},"rehauRel12");
        lesFiltres.put("rehauRel12",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0.416,0.416,0.416,0,0.416,0.416,0.416,0,0.416,0.416,0.416},"rehauRel25");
        lesFiltres.put("rehauRel25",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{0,0.291,0.291,0.291,0,0.291,0.291,0.291,0,0.291,0.291,0.291},"assombRel12");
        lesFiltres.put("assombRel12",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0.250,0.250,0.250,0,0.250,0.250,0.250,0,0.250,0.250,0.250},"assombRel25");
        lesFiltres.put("assombRel25",filtrePredf);


        //Filtres couleurs
        filtrePredf=new FiltreLineaire(new double[]{255,-1,0,0,255,0,-1,0,255,0,0,-1},"negCouleur");
        lesFiltres.put("negCouleur",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1.25,0,0,0,0,1.125,0,0,0,0,1.25},"rehauCouleur25");
        lesFiltres.put("rehauCouleur25",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{0,1.25,0,0,0,0,1,0,0,0,0,1},"rehauRouge25");
        lesFiltres.put("rehauRouge25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1,0,0,0,0,1.25,0,0,0,0,1},"rehauVert25");
        lesFiltres.put("rehauVert25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1,0,0,0,0,1,0,0,0,0,1.25},"rehauBleu25");
        lesFiltres.put("rehauBleu25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1.25,0,0,0,0,1.25,0,0,0,0,1},"rehauJaune25");
        lesFiltres.put("rehauJaune25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1,0,0,0,0,1.25,0,0,0,0,1.25},"rehauCyan25");
        lesFiltres.put("rehauCyan25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1.25,0,0,0,0,1,0,0,0,0,1.25},"rehauMagenta25");
        lesFiltres.put("rehauMagenta25",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{0,0.75,0,0,0,0,0.75,0,0,0,0,0.75},"assombCoul25");
        lesFiltres.put("assombCoul25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0.75,0,0,0,0,1,0,0,0,0,1},"assombRouge25");
        lesFiltres.put("assombRouge25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1,0,0,0,0,0.75,0,0,0,0,1},"assombVert25");
        lesFiltres.put("assombVert25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1,0,0,0,0,1,0,0,0,0,0.75},"assombBleu25");
        lesFiltres.put("assombBleu25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0.75,0,0,0,0,0.75,0,0,0,0,1},"assombJaune25");
        lesFiltres.put("assombJaune25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,1,0,0,0,0,0.75,0,0,0,0,0.75},"assombCyan25");
        lesFiltres.put("assombCyan25",filtrePredf);
        filtrePredf=new FiltreLineaire(new double[]{0,0.75,0,0,0,0,1,0,0,0,0,0.75},"assombMagenta25");
        lesFiltres.put("assombMagenta25",filtrePredf);

        filtrePredf=new FiltreLineaire(new double[]{0,0.393,0.769,0.189,0,0.349,0.686,0.168,0,0.272,0.534,0.131},"sepia");
        lesFiltres.put("sepia",filtrePredf);

        return lesFiltres;
    }

    //Création des filtres gammas prédéfinis
    public HashMap<String,FiltreGamma> creationFiltreGammaPredef() {
        HashMap<String, FiltreGamma> lesFiltres = new HashMap<String, FiltreGamma>();

        FiltreGamma filtrePredf = new FiltreGamma(new double[]{0.0, 1.0, 0.75}, "gammaRehauNiv1");
        lesFiltres.put("gammaRehauNiv1", filtrePredf);

        filtrePredf = new FiltreGamma(new double[]{0.0, 1.0, 0.50}, "gammaRehauNiv2");
        lesFiltres.put("gammaRehauNiv2", filtrePredf);
        filtrePredf = new FiltreGamma(new double[]{0.0, 1.0, 0.25}, "gammaRehauNiv3");
        lesFiltres.put("gammaRehauNiv3", filtrePredf);

        filtrePredf = new FiltreGamma(new double[]{0.0, 1.0, 1.50}, "gammaAssombNiv1");
        lesFiltres.put("gammaAssombNiv1", filtrePredf);
        filtrePredf = new FiltreGamma(new double[]{0.0, 1.0, 2.0}, "gammaAssombNiv1");
        lesFiltres.put("gammaAssombNiv1", filtrePredf);
        filtrePredf = new FiltreGamma(new double[]{0.0, 1.0, 4.0}, "gammaAssombNiv1");
        lesFiltres.put("gammaAssombNiv1", filtrePredf);

        filtrePredf = new FiltreGamma(new double[]{255.0, -1.0, 1.0}, "gammaNegatif");
        lesFiltres.put("gammaNegatif", filtrePredf);

        filtrePredf = new FiltreGamma(new double[]{255.0, -1.0, 0.75}, "gammaRehauNiv1Neg");
        lesFiltres.put("gammaRehauNiv1Neg", filtrePredf);
        filtrePredf = new FiltreGamma(new double[]{255.0, -1.0, 0.50}, "gammaRehauNiv2Neg");
        lesFiltres.put("gammaRehauNiv2Neg", filtrePredf);
        filtrePredf = new FiltreGamma(new double[]{255.0, -1, 0.25}, "gammaRehauNiv3Neg");
        lesFiltres.put("gammaRehauNiv3Neg", filtrePredf);

        filtrePredf = new FiltreGamma(new double[]{255.0, -1.0, 1.5}, "gammaAssombNiv1Neg");
        lesFiltres.put("gammaAssombNiv1Neg", filtrePredf);
        filtrePredf = new FiltreGamma(new double[]{255.0, -1.0, 2.0}, "gammaAssombNiv2Neg");
        lesFiltres.put("gammaAssombNiv2Neg", filtrePredf);
        filtrePredf = new FiltreGamma(new double[]{255.0, -1.0, 4.0}, "gammaAssombNiv3Neg");
        lesFiltres.put("gammaAssombNiv3Neg", filtrePredf);

        return  lesFiltres;
    }

//Création des filtres convolution prédéfinis

    public HashMap<String,FiltreConvolution> creationFiltreConvolutionPredef() {
        HashMap<String, FiltreConvolution> lesFiltres = new HashMap<String, FiltreConvolution>();

        FiltreConvolution filtrePredf = new FiltreConvolution(new double[]{0.111,0.111,0.111,
                0.111,0.111,0.111,
                0.111,0.111,0.111},"moyenUniforme3");
        lesFiltres.put("moyenUniforme3", filtrePredf);

        filtrePredf = new FiltreConvolution(new double[]{0.04,0.04,0.04,0.04,0.04,
                0.04,0.04,0.04,0.04,0.04,
                0.04,0.04,0.04,0.04,0.04,
                0.04,0.04,0.04,0.04,0.04,
                0.04,0.04,0.04,0.04,0.04},"moyenUniforme5");
        lesFiltres.put("moyenUniforme5", filtrePredf);

        filtrePredf = new FiltreConvolution(new double[]{0.02,0.02,0.02,0.02,0.02,0.02,0.02,
                0.02,0.02,0.02,0.02,0.02,0.02,0.02,
                0.02,0.02,0.02,0.02,0.02,0.02,0.02,
                0.02,0.02,0.02,0.02,0.02,0.02,0.02,
                0.02,0.02,0.02,0.02,0.02,0.02,0.02,
                0.02,0.02,0.02,0.02,0.02,0.02,0.02,
                0.02,0.02,0.02,0.02,0.02,0.02,0.02},"moyenUniforme7");
        lesFiltres.put("moyenUniforme7", filtrePredf);

        filtrePredf = new FiltreConvolution(new double[]{0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,
                0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,
                0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,
                0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,
                0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,
                0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,
                0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,
                0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,
                0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012,0.012},"moyenUniform9");
        lesFiltres.put("moyenUniform9", filtrePredf);

        filtrePredf = new FiltreConvolution(new double[]{0.0625,0.125,0.0625,
                0.125,0.25,0.125,
                0.0625,0.125,0.0625},"gaussien3");
        lesFiltres.put("gaussien3", filtrePredf);

        filtrePredf= new FiltreConvolution(new double[]{0.0037,0.0147,0.0256,0.0147,0.0037,
                0.0147,0.0586,0.0952,0.0586,0.0147,
                0.0256,0.0952,0.1502,0.0952,0.0256,
                0.0147,0.0586,0.0952,0.0586,0.0147,
                0.0037,0.0147,0.0256,0.0147,0.0037},"gaussien5");
        lesFiltres.put("gaussien5", filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,0,0,-1,1,0,0,0},"directDroite");
        lesFiltres.put("directDroite",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,0,0,-1,0,0,0,1},"directDroiteBas");
        lesFiltres.put("directDroiteBas",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,0,0,-1,0,0,1,0},"directBas");
        lesFiltres.put("directBas",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,0,0,-1,0,1,0,0},"directBasGauche");
        lesFiltres.put("directBasGauche",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,0,1,-1,0,0,0,0},"directGauche");
        lesFiltres.put("directGauche",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {1,0,0,0,-1,0,0,0,0},"directGaucheHaut");
        lesFiltres.put("directGaucheHaut",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,1,0,0,-1,0,0,0,0},"directHaut");
        lesFiltres.put("directHaut",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,1,0,-1,0,0,0,0},"directHautDroite");
        lesFiltres.put("directHautDroite",filtrePredf);

        //Sorbel

        filtrePredf= new FiltreConvolution(new double[] {-1,0,1,-2,0,2,-1,0,1},"sorbelDroite");
        lesFiltres.put("sorbelDroite",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {-2,-1,0,-1,0,1,0,1,2},"sorbelDroiteBas");
        lesFiltres.put("sorbelDroiteBas",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {-1,-2,-1,0,0,0,1,2,1},"sorbelBas");
        lesFiltres.put("sorbelBas",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,-1,-2,1,0,-1,2,1,0},"sorbelBasGauche");
        lesFiltres.put("sorbelBasGauche",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {1,0,-1,2,0,-2,1,0,-1},"sorbelGauche");
        lesFiltres.put("sorbelGauche",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {2,1,0,1,0,-1,0,-1,-2},"sorbelGaucheHaut");
        lesFiltres.put("sorbelGaucheHaut",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {1,2,1,0,0,0,-1,-2,-1},"sorbelHaut");
        lesFiltres.put("sorbelHaut",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,1,2,-1,0,1,-2,-1,0},"sorbelHautDroite");
        lesFiltres.put("sorbelHautDroite",filtrePredf);


        //Embrossage
        filtrePredf= new FiltreConvolution(new double[] {0,0,0,-1,-1,2,0,0,0},"embroDroite");
        lesFiltres.put("embroDroite",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {-1,0,0,0,-1,0,0,0,2},"embroDroiteBas");
        lesFiltres.put("embroDroiteBas",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,-1,0,0,-1,0,0,2,0},"embroBas");
        lesFiltres.put("embroBas",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,-1,0,-1,0,2,0,0},"embroBasGauche");
        lesFiltres.put("embroBasGauche",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,0,2,-1,-1,0,0,0},"embroGauche");
        lesFiltres.put("embroGauche",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {2,0,0,0,-1,0,0,0,-1},"embroGaucheHaut");
        lesFiltres.put("embroGaucheHaut",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,2,0,0,-1,0,0,-1,0},"embroHaut");
        lesFiltres.put("embroHaut",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {0,0,2,0,-1,0,-1,0,0},"embroHautDroite");
        lesFiltres.put("embroHautDroite",filtrePredf);


        //Autres
        filtrePredf= new FiltreConvolution(new double[] {0,1,0,1,-4,1,0,1,0},"edgeDetect");
        lesFiltres.put("edgeDetect",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {-1,-1,-1,-1,9,-1,-1,-1,-1},"sharpness");
        lesFiltres.put("sharpness",filtrePredf);

        filtrePredf= new FiltreConvolution(new double[] {1,1,1,1,-8,1,1,1,1},"embroUni");
        lesFiltres.put("embroUni",filtrePredf);

        return lesFiltres;

    }
}
