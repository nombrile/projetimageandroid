package com.example.vincenta.projetfinaldec;


import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.vincenta.projetfinaldec.RecyclerViewClasses.FragmentListeTypeFiltres;
import com.example.vincenta.projetfinaldec.RecyclerViewClasses.FragmentRecyclerFiltres;
import com.example.vincenta.projetfinaldec.activityFiltresCustom.FiltreConvoCustomActivity;
import com.example.vincenta.projetfinaldec.activityFiltresCustom.FiltreConvolutionCustomChoixNbParams;
import com.example.vincenta.projetfinaldec.activityFiltresCustom.FiltreGammaCustomActivity;
import com.example.vincenta.projetfinaldec.activityFiltresCustom.FiltreLineaireCustomActvity;
import com.example.vincenta.projetfinaldec.lesFragmenta.FragmentBoutonsAjustements;
import com.example.vincenta.projetfinaldec.lesFragmenta.FragmentMenuChoixOutils;
import com.example.vincenta.projetfinaldec.lesFragmenta.FragmentRotation;
import com.example.vincenta.projetfinaldec.lesFragmenta.FragmentSeekBar;
import com.example.vincenta.projetfinaldec.lesFragmenta.FragmentSousMenuOptionsAjust;
import com.example.vincenta.projetfinaldec.outilsTransformation.CapturePhotoUtils;
import com.example.vincenta.projetfinaldec.outilsTransformation.HistoriqueImage;
import com.example.vincenta.projetfinaldec.outilsTransformation.OutilsEdition;
import com.example.vincenta.projetfinaldec.outilsTransformation.OutilsExternes;
import com.example.vincenta.projetfinaldec.outilsTransformation.SauvegardeImageAvantModif;
import java.util.HashMap;


public class EditingActivity extends AppCompatActivity implements FragmentMenuChoixOutils.OnFragmentInteractionListener,
        FragmentSousMenuOptionsAjust.OnFragmentInteractionListener,FragmentSeekBar.OnFragmentInteractionListener,
        SauvegardeImageAvantModif, FragmentBoutonsAjustements.OnFragmentInteractionListener,
        FragmentRotation.OnFragmentInteractionListener, FragmentListeTypeFiltres.OnFragmentInteractionListener,
        FragmentRecyclerFiltres.OnFragmentInteractionListener
{

    HashMap<String,HashMap> lesFiltresPredef;

    OutilsEdition lesOutils;
    ImageView imgChoisie;

    CapturePhotoUtils sauveurDePhoto=new CapturePhotoUtils();
    int btnAjustementChoisi;
    Bitmap imgSaver;
    Filtre filtreCustom;
    static final int FILTRE_LIN_CUSTOM_REQUEST=444;
    static final int FILTRE_GAMMA_CUSTOM_REQUEST=555;
    static final int FILTRE_CONVO_NBPARAM_REQUEST=666;
    static final int FILTRE_CONVO_CUSTOM_REQUEST=777;

    Bitmap imgThumbnail;

    OutilsExternes outilsExternes;

    FragmentMenuChoixOutils menuOutils=new FragmentMenuChoixOutils();
    FragmentSousMenuOptionsAjust optionsAjustables=new FragmentSousMenuOptionsAjust();

    public HashMap<String, HashMap> getLesFiltresPredef() {
        return lesFiltresPredef;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editing);
        getSupportActionBar().show();

        //Creation des outils
        lesOutils=new OutilsEdition();
        outilsExternes=new OutilsExternes();
        imgChoisie=(ImageView)findViewById(R.id.editImgPrincipale);

        //Creation des filtres
        lesFiltresPredef=new HashMap<>();
        lesFiltresPredef.put("filtresLineaires",lesOutils.creationFiltreLinPredef());
        lesFiltresPredef.put("filtresGamma",lesOutils.creationFiltreGammaPredef());
        lesFiltresPredef.put("filtresConvolution", lesOutils.creationFiltreConvolutionPredef());



/*****************À appliquer au départ, ne s'applique plus lorsqu'on tourne l'écran*******************/
        if(savedInstanceState == null) {
            //Implentation des fragments de départ
            FragmentManager manager=getSupportFragmentManager();
            FragmentTransaction transaction=manager.beginTransaction();
            //le menu outils sera tjrs le même
            transaction.add(R.id.editMenuEdition, menuOutils);
            //les options disponible vont changer selon outils choisi
            transaction.add(R.id.editOptionsChoixMenu, optionsAjustables);
            transaction.commit();
            //Met image recu dans ImageView
            Bitmap imgRecue=outilsExternes.decompressImage(getIntent().getByteArrayExtra("imagebyte"));
            //Et l'ajoute dans l'historique
            HistoriqueImage.ajouterImgHistorique(imgRecue);
            imgChoisie.setImageBitmap(imgRecue);
        }

    }

/*****************Methode pour tout garder lorsqu'on tourne l'écran*******************/
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("image",(HistoriqueImage.obtenirImgHistorique()));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        imgChoisie.setImageBitmap((Bitmap) savedInstanceState.getParcelable("image"));
        imgSaver=savedInstanceState.getParcelable("image");
    }

    /********************************Options pour ActionBar (bar au top)*************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_editing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id==android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.editBtnRestart){
            imgChoisie.setImageBitmap(HistoriqueImage.restartHistorique());
            imgSaver=HistoriqueImage.obtenirImgHistorique();
        }
        if (id == R.id.editBtnRevert) {
            imgChoisie.setImageBitmap(HistoriqueImage.revertImgHistorique());
            imgSaver=HistoriqueImage.obtenirImgHistorique();
        }
        if (id == R.id.editBtnSaveImg) {

            sauveurDePhoto.insertImage(getApplicationContext().getContentResolver(), ((BitmapDrawable) imgChoisie.getDrawable()).getBitmap(), "monimage", "description");
            Toast.makeText(getApplicationContext(),"Image sauvegardée",Toast.LENGTH_LONG).show();
            Intent i=new Intent(EditingActivity.this,StartActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /********************************Options pour ActionBar (bar au top)*************************/

    //Fonction ajustement lorsqu'on modifie contrast, hue, saturation luminosité avec progress bar
    @Override
    public void onProgressChange(int progress) {
        Bitmap imgUtilise=imgSaver.copy(imgSaver.getConfig(), true);
        double valAjustement=((double)progress)/100.0;
        AsyncTaskRunner asyncTaskRunner=new AsyncTaskRunner();
        if(btnAjustementChoisi==R.id.ajustBtnContrast) {
            asyncTaskRunner.execute("progress", imgUtilise, (double) progress);
        }
        else{

            if(btnAjustementChoisi==R.id.ajustBtnHue)
                asyncTaskRunner.execute("progress2", imgUtilise, valAjustement, 0);

            else if(btnAjustementChoisi==R.id.ajustBtnSaturation)
                asyncTaskRunner.execute("progress2", imgUtilise, valAjustement, 1);

            else if(btnAjustementChoisi==R.id.ajustBtnLuminosite)
                asyncTaskRunner.execute("progress2", imgUtilise, valAjustement, 2);
        }

    }

    //Decision pour les ajustements
    @Override
    public void decisionAjustement(View v) {
        int btnChoisi=v.getId();
        if(btnChoisi==R.id.btnAjustementOk){
            appliquerModif();
        }
        if(btnChoisi==R.id.btnAjustementCancel){
            annulerModif();
        }
        modifierLesMenus(menuOutils,optionsAjustables);
        getSupportActionBar().show();
    }


    //Action lorsqu'on utilise les boutons pour la rotation
    @Override
    public void rotationImg(View v) {
        if(v.getId()==R.id.fragRotationbtnRotation){
            Bitmap imgUtilise=imgSaver.copy(imgSaver.getConfig(),true);
            imgSaver=lesOutils.rotationImg(imgUtilise);
            imgChoisie.setImageBitmap(imgSaver);
        }
    }


    //Fait une sauvegarde de l'image actuel avant une modification pour l'utiliser
    @Override
    public void btnFragmentMenuChoixOutils(View v) {
        btnAjustementChoisi=v.getId();
        imgSaver=((BitmapDrawable) imgChoisie.getDrawable()).getBitmap().copy(((BitmapDrawable) imgChoisie.getDrawable()).getBitmap().getConfig(), false);
    }

    @Override
    public void applicationFiltre(String nomFiltre, String typeFiltre) {

        if(nomFiltre.equals("Custom")){
           applicationFiltreCustom(typeFiltre);
        }

        else {
            filtreCustom = (Filtre) lesFiltresPredef.get(typeFiltre).get(nomFiltre);
            AsyncTaskRunner asyncTaskRunner=new AsyncTaskRunner();
            asyncTaskRunner.execute("filtre");
        }

    }

    //Fonction pour modifier les menu afficher
    public void modifierLesMenus(Fragment menuHaut, Fragment menuBas){
        FragmentManager manager=getSupportFragmentManager();
        FragmentTransaction transaction=manager.beginTransaction();
        //le menu outils sera tjrs le même
        transaction.replace(R.id.editMenuEdition, menuHaut);

        //les options disponible vont changer selon outils choisi
        transaction.replace(R.id.editOptionsChoixMenu, menuBas);
        transaction.commit();
    }

    //Appliquer definitivement un modif
    public void appliquerModif(){
        HistoriqueImage.ajouterImgHistorique(((BitmapDrawable) imgChoisie.getDrawable()).getBitmap());
        imgChoisie.setImageBitmap(HistoriqueImage.obtenirImgHistorique());
    }

    //Ne pas appliquer la modif à l'essai
    public void annulerModif(){
        imgChoisie.setImageBitmap(HistoriqueImage.obtenirImgHistorique());
        imgSaver=HistoriqueImage.obtenirImgHistorique();
    }


    ///3 methodes pour la creation et application de filtres custom


    @Override
    public void choixTypeFiltreAfficher(String typeFiltre) {
        imgThumbnail=outilsExternes.scaleBitmap(((BitmapDrawable)imgChoisie.getDrawable()).getBitmap(),40,getApplicationContext());
        if(typeFiltre.equals("filtresLineaires"))
            modifierLesMenus(FragmentRecyclerFiltres.newInstance("filtresLineaires",imgThumbnail),new FragmentBoutonsAjustements());

        else if(typeFiltre.equals("filtresGamma"))
            modifierLesMenus(FragmentRecyclerFiltres.newInstance("filtresGamma", imgThumbnail),new FragmentBoutonsAjustements());

        else
            modifierLesMenus(FragmentRecyclerFiltres.newInstance("filtresConvolution", imgThumbnail),new FragmentBoutonsAjustements());

        getSupportActionBar().hide();
    }

    public void applicationFiltreCustom(String typeDeFiltre){

        Intent i;
        if(typeDeFiltre.equals("filtresLineaires")){
            i=new Intent(EditingActivity.this, FiltreLineaireCustomActvity.class);
            startActivityForResult(i,FILTRE_LIN_CUSTOM_REQUEST);
        }
        else if(typeDeFiltre.equals("filtresGamma")){
            i=new Intent(EditingActivity.this, FiltreGammaCustomActivity.class);
            startActivityForResult(i,FILTRE_GAMMA_CUSTOM_REQUEST);
        }
        else if(typeDeFiltre.equals("filtresConvolution")){
            i=new Intent(EditingActivity.this, FiltreConvolutionCustomChoixNbParams.class);
            startActivityForResult(i,FILTRE_CONVO_NBPARAM_REQUEST);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK){

            if(requestCode==FILTRE_CONVO_NBPARAM_REQUEST){
                int nbParams=data.getIntExtra("nbParams",3);
                creationFiltreConvo(nbParams);
            }

            else {
                String choix = data.getStringExtra("choix");

                if (choix.equals("ok")) {

                    if (requestCode == FILTRE_LIN_CUSTOM_REQUEST) {
                        double[] lesParamsCustom = data.getDoubleArrayExtra("lesParams");
                        filtreCustom = new FiltreLineaire(lesParamsCustom, "custom");
                    }

                    if (requestCode == FILTRE_GAMMA_CUSTOM_REQUEST) {
                        double[] lesParamsCustom = data.getDoubleArrayExtra("lesParams");
                        filtreCustom = new FiltreGamma(lesParamsCustom, "custom");
                    }

                    if(requestCode==FILTRE_CONVO_CUSTOM_REQUEST){
                        double[] lesParamsCustom = data.getDoubleArrayExtra("lesParams");
                        filtreCustom = new FiltreConvolution(lesParamsCustom, "custom");
                    }
                    AsyncTaskRunner asyncTaskRunner=new AsyncTaskRunner();
                    asyncTaskRunner.execute("filtre");
                }
            }
        }
    }

    private void creationFiltreConvo(int nbParams){
        Intent i=new Intent(EditingActivity.this, FiltreConvoCustomActivity.class);
        i.putExtra("nbParams", nbParams);
        startActivityForResult(i, FILTRE_CONVO_CUSTOM_REQUEST);

    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    private class AsyncTaskRunner extends AsyncTask {

        Bitmap imgFiltre = imgSaver.copy(imgSaver.getConfig(), true);
        Bitmap imageResultante;
        @Override
        protected Object doInBackground(Object[] params) {

            publishProgress();

            if(params[0]=="progress"){
                imageResultante=lesOutils.adjustContrast((Bitmap) params[1], (Double) params[2]);
            }
            else if(params[0]=="progress2"){
                    imageResultante=lesOutils.chgHslImage((Bitmap)params[1], (Double)params[2],(Integer)params[3]);
            }

            else
            imageResultante=filtreCustom.tranformation(imgFiltre);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imgChoisie.setImageBitmap(imageResultante);
                }
            });
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            imgChoisie.clearAnimation();
        }

        @Override
        protected void onProgressUpdate(Object[] values) {
            super.onProgressUpdate(values);
            Bitmap image=BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.loadingmark);
            imgChoisie.setImageBitmap(image);
            Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotation);
            imgChoisie.startAnimation(startRotateAnimation);

        }
    }
}
