package com.example.vincenta.projetfinaldec.activityFiltresCustom;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.vincenta.projetfinaldec.R;

public class FiltreLineaireCustomActvity extends AppCompatActivity {

    EditText parametre1,parametre2,parametre3,parametre4,parametre5,parametre6,parametre7,parametre8,
    parametre9,parametre10,parametre11,parametre12;
    Ecouteur ec;

    ImageButton btnOkLineaireCustom;
    ImageButton btnCancelLineaireCustom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filtre_lineaire_custom_actvity);

        setupVariables();
    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {

            if(v==btnOkLineaireCustom){
                if(champsVide()){
                    Toast.makeText(getApplicationContext(), "Remplissez tous les champs", Toast.LENGTH_LONG).show();
                }
                else{
                    double[] lesParams=creationDesParamètre();
                    Intent i=new Intent();
                    i.putExtra("lesParams",lesParams);
                    i.putExtra("choix","ok");
                    setResult(RESULT_OK,i);
                    finish();
                }
            }
            if(v==btnCancelLineaireCustom){
                Intent i=new Intent();
                i.putExtra("choix","cancel");
                setResult(RESULT_OK,i);
                finish();
            }
        }
    }

    private boolean champsVide(){
        int longParam1=parametre1.getText().toString().trim().length();
        int longParam2=parametre2.getText().toString().trim().length();
        int longParam3=parametre3.getText().toString().trim().length();
        int longParam4=parametre4.getText().toString().trim().length();
        int longParam5=parametre5.getText().toString().trim().length();
        int longParam6=parametre6.getText().toString().trim().length();
        int longParam7=parametre7.getText().toString().trim().length();
        int longParam8=parametre8.getText().toString().trim().length();
        int longParam9=parametre9.getText().toString().trim().length();
        int longParam10=parametre10.getText().toString().trim().length();
        int longParam11=parametre11.getText().toString().trim().length();
        int longParam12=parametre12.getText().toString().trim().length();
        if(longParam1==0 || longParam2==0 || longParam3==0 || longParam4==0 || longParam5==0 ||
                longParam6==0 || longParam7==0 || longParam8==0 || longParam9==0 || longParam10==0
                || longParam11==0 || longParam12==0)
            return true;
        return false;
    }


    private void setupVariables(){

        parametre1=(EditText)findViewById(R.id.param1LinId);
        parametre2=(EditText)findViewById(R.id.param2LinId);
        parametre3=(EditText)findViewById(R.id.param3LinId);
        parametre4=(EditText)findViewById(R.id.param4LinId);
        parametre5=(EditText)findViewById(R.id.param5LinId);
        parametre6=(EditText)findViewById(R.id.param6LinId);
        parametre7=(EditText)findViewById(R.id.param7LinId);
        parametre8=(EditText)findViewById(R.id.param8LinId);
        parametre9=(EditText)findViewById(R.id.param9LinId);
        parametre10=(EditText)findViewById(R.id.param10LinId);
        parametre11=(EditText)findViewById(R.id.param11LinId);
        parametre12=(EditText)findViewById(R.id.param12LinId);

        ec=new Ecouteur();
        btnOkLineaireCustom=(ImageButton)findViewById(R.id.paramsLineareCustomBtnOk);
        btnCancelLineaireCustom=(ImageButton)findViewById(R.id.paramsLineareCustomBtnCancel);

        btnOkLineaireCustom.setOnClickListener(ec);
        btnCancelLineaireCustom.setOnClickListener(ec);
    }

    private double[] creationDesParamètre(){
        double param1=Double.parseDouble(parametre1.getText().toString());
        double param2=Double.parseDouble(parametre2.getText().toString());
        double param3=Double.parseDouble(parametre3.getText().toString());
        double param4=Double.parseDouble(parametre4.getText().toString());
        double param5=Double.parseDouble(parametre5.getText().toString());
        double param6=Double.parseDouble(parametre6.getText().toString());
        double param7=Double.parseDouble(parametre7.getText().toString());
        double param8=Double.parseDouble(parametre8.getText().toString());
        double param9=Double.parseDouble(parametre9.getText().toString());
        double param10=Double.parseDouble(parametre10.getText().toString());
        double param11=Double.parseDouble(parametre11.getText().toString());
        double param12=Double.parseDouble(parametre12.getText().toString());
        double[] lesParams={param1,param2,param3,param4,param5,param6,param7,param8,param9,param10,param11,param12};
        return lesParams;
    }
}
