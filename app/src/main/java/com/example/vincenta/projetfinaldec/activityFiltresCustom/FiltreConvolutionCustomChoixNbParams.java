package com.example.vincenta.projetfinaldec.activityFiltresCustom;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.vincenta.projetfinaldec.R;

public class FiltreConvolutionCustomChoixNbParams extends AppCompatActivity {

    Button btn3x3Param;
    Button btn5x5Param;
    Ecouteur ec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtre_convolution_custom_choix_nb_params);
        setupVariables();
    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            Intent i=new Intent();
            if(v==btn3x3Param)
                i.putExtra("nbParams",3);
            if(v==btn5x5Param)
                i.putExtra("nbParams",5);

            setResult(RESULT_OK,i);
            finish();
        }
    }

    private void setupVariables(){
        btn3x3Param=(Button)findViewById(R.id.btnTailleConvo3);
        btn5x5Param=(Button)findViewById(R.id.btnTailleConvo5);
        ec=new Ecouteur();

        btn3x3Param.setOnClickListener(ec);
        btn5x5Param.setOnClickListener(ec);

    }
}
